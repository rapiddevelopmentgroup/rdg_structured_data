<?php

namespace Drupal\rdg_structured_data;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;

/**
 * Class StructuredData.
 */
class StructuredData implements StructuredDataInterface {

  /**
   * Constructs a new StructuredData object.
   */
  public function __construct(
    protected RendererInterface $renderer,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function prepareJsonRenderArray(array $main_content) : array {
    $render_context = new RenderContext();
    $this->renderer->executeInRenderContext($render_context, function () use ($main_content) {
      $this->renderer->render($main_content);
    });
    $bubbleable_metadata = $render_context->pop();
    $attachments = $bubbleable_metadata->getAttachments();
    $render_array = [];

    if (!empty($attachments['drupalSettings']['rdg_structured_data'])) {
      $structured_data = [];
      foreach ($attachments['drupalSettings']['rdg_structured_data'] as $schema_info) {
        $merge_array = [$schema_info['key'] => $schema_info['data']];
        $structured_data = $this->arrayMergeRecursiveDistinct($structured_data, $merge_array);
      }

      foreach ($structured_data as $data) {
        $render_array[] = [
          '#theme' => 'rdg_structured_data',
          '#json' => Json::encode($data),
        ];
      }
    }

    return $render_array;
  }

  /**
   * Modified array_merge_recursive without changing datatypes.
   *
   * Matching keys' values in the second array overwrite those in the first
   * array, as is the case with array_merge.
   *
   * Parameters are passed by reference, though only for performance reasons.
   * They're not altered by this function.
   *
   * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
   * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
   */
  protected function arrayMergeRecursiveDistinct(array &$array1, array &$array2) {
    $merged = $array1;

    foreach ($array2 as $key => &$value) {
      if (is_int($key)) {
        $merged[] = $value;
      }
      else {
        if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
          $merged[$key] = $this->arrayMergeRecursiveDistinct($merged[$key], $value);
        }
        else {
          $merged[$key] = $value;
        }
      }
    }

    return $merged;
  }

}
