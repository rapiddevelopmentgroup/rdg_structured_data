<?php

namespace Drupal\rdg_structured_data\Plugin\ExtraField\Display;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Schema fields.
 */
abstract class SchemaBase extends ExtraFieldDisplayBase implements ContainerFactoryPluginInterface {

  /**
   * The Drupal Core UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuidService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $plugin->uuidService = $container->get('uuid');

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {
    $output = [];
    $uuid = $this->uuidService->generate();

    $output['schema'] = [
      '#attached' => [
        'drupalSettings' => [
          'rdg_structured_data' => [
            $uuid => [
              'key' => $this->key(),
              'data' => $this->data($entity),
            ],
          ],
        ],
      ],
    ];

    return $output;
  }

  /**
   * Generate the key to store the data.
   *
   * This key is used to keep like data together; for example, in the scenario
   * of a FAQPage element, each individual FAQ entity would like to provide
   * some content that is merged together into a single FAQPage with multiple
   * values inside its mainEntity array. To accomplish this, they share a key
   * of "faq" so they are merged together.
   */
  abstract protected function key() : string;

  /**
   * Generate the data for the entity.
   */
  abstract protected function data(EntityInterface $entity) : array;

}
