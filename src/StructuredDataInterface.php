<?php

namespace Drupal\rdg_structured_data;

/**
 * Service for extracting JSON+LD data from metadata.
 */
interface StructuredDataInterface {

  /**
   * Get the JSON-LD data corresponding to a page's render array.
   */
  public function prepareJsonRenderArray(array $main_content) : array;

}
